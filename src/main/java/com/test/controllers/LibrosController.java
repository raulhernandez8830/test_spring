package com.test.controllers;

import com.test.models.LibroEntity;
import com.test.services.LibroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
public class LibrosController {

    @Autowired
    private LibroService libroService;

    @GetMapping("/libros")
    public List<LibroEntity> findAll(){
        List<LibroEntity> libros = this.libroService.findAll();
        return libros;
    }

    @PostMapping("/libros")
    public LibroEntity save(@RequestBody LibroEntity libro){
        LibroEntity l = this.libroService.save(libro);
        return l;
    }


    @PutMapping("/libros/{id}")
    public LibroEntity update(@PathVariable("id") Integer id, @RequestBody LibroEntity libro){
        LibroEntity l = this.libroService.update(libro,id);
        return l;
    }


}
