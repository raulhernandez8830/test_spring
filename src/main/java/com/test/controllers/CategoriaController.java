package com.test.controllers;

import com.test.models.CategoriaEntity;
import com.test.services.CategoriaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.PUT, RequestMethod.POST})
public class CategoriaController {

    @Autowired
    CategoriaService categoriaService;

    @GetMapping("/categorias")
    public List<CategoriaEntity> findAll(){
        List<CategoriaEntity> categorias = this.categoriaService.findAll();
        return categorias;
    }

    @PostMapping("/categorias")
    public CategoriaEntity save(@RequestBody CategoriaEntity c){
        CategoriaEntity categoria = this.categoriaService.save(c);
        return categoria;
    }

    @PutMapping("/categorias/{id}")
    public CategoriaEntity update(@PathVariable("id") Integer id, @RequestBody CategoriaEntity c){
        CategoriaEntity categoria = this.categoriaService.update(c, id);
        return categoria;
    }
}
