package com.test.controllers;

import com.test.models.AutorEntity;
import com.test.services.AutorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
public class AutorController {

    @Autowired
    AutorService autorService;

    @GetMapping("/autores")
    public List<AutorEntity> findAll(){
        List<AutorEntity> autores = this.autorService.findAll();
        return autores;
    }

    @PostMapping("/autores")
    public AutorEntity save(@RequestBody AutorEntity a ){
        AutorEntity autor = this.autorService.save(a);
        return autor;
    }

    @PutMapping("/autores/{id}")
    public AutorEntity update(@PathVariable("id") Integer id, @RequestBody AutorEntity a){
        AutorEntity autor = this.autorService.update(a,id);
        return autor;
    }
}
