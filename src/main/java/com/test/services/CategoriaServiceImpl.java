package com.test.services;

import com.test.dao.CategoriaDAO;
import com.test.models.CategoriaEntity;
import jakarta.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoriaServiceImpl implements CategoriaService {

    private EntityManager em;

    @Autowired
    private CategoriaDAO categoriaDAO;


    @Override
    public List<CategoriaEntity> findAll() {
        List<CategoriaEntity> categorias = (List<CategoriaEntity>) this.categoriaDAO.findAll();
        return categorias;
    }

    @Override
    public CategoriaEntity save(CategoriaEntity categoria) {
        return this.categoriaDAO.save(categoria);
    }

    @Override
    public CategoriaEntity update(CategoriaEntity categoria, Integer id) {
        CategoriaEntity c = this.categoriaDAO.findById(id).get();
        c = categoria;
        return this.categoriaDAO.save(c);
    }
}
