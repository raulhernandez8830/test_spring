package com.test.services;

import com.test.models.LibroEntity;

import java.util.List;

public interface LibroService {

    public List<LibroEntity> findAll();
    public LibroEntity save(LibroEntity libro);

    public LibroEntity update(LibroEntity libro, Integer id);
}
