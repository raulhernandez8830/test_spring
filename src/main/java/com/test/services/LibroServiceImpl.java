package com.test.services;

import com.test.dao.LibroDAO;
import com.test.models.LibroEntity;
import jakarta.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LibroServiceImpl implements LibroService {

    @Autowired
    private  LibroDAO libroDAO;
    private EntityManager em;

    @Override
    public List<LibroEntity> findAll() {
       List<LibroEntity> libros = (List<LibroEntity>) this.libroDAO.findAll();
       return libros;
    }

    @Override
    public LibroEntity save(LibroEntity libro) {
        return this.libroDAO.save(libro);
    }

    @Override
    public LibroEntity update(LibroEntity libro, Integer id) {
        LibroEntity l = this.libroDAO.findById(id).get();
        l = libro;
        return this.libroDAO.save(l);
    }
}
