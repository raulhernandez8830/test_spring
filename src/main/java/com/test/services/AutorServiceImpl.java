package com.test.services;

import com.test.dao.AutorDAO;
import com.test.models.AutorEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AutorServiceImpl implements AutorService {

    @Autowired
    AutorDAO autorDAO;


    @Override
    public List<AutorEntity> findAll() {
        List<AutorEntity> autores = (List<AutorEntity>) this.autorDAO.findAll();
        return autores;
    }

    @Override
    public AutorEntity save(AutorEntity autor) {
        AutorEntity a  = this.autorDAO.save(autor);
        return a;
    }

    @Override
    public AutorEntity update(AutorEntity autor, Integer id) {
        AutorEntity a = this.autorDAO.findById(id).get();
        a = autor;
        return this.autorDAO.save(a);
    }
}
