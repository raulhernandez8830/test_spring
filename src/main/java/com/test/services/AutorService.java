package com.test.services;

import com.test.models.AutorEntity;

import java.util.List;

public interface AutorService {

    public List<AutorEntity> findAll();
    public AutorEntity save(AutorEntity autor);
    public AutorEntity update(AutorEntity autor, Integer id);


}
