package com.test.services;

import com.test.models.CategoriaEntity;

import java.util.List;

public interface CategoriaService {

    public List<CategoriaEntity> findAll();
    public CategoriaEntity save(CategoriaEntity categoria);
    public CategoriaEntity update(CategoriaEntity categoria, Integer id);


}
