package com.test.models;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="libros")
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class LibroEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String titulo;
    private Boolean estado;

    @ManyToOne
    @JoinColumn(name="autor_id")
    private AutorEntity autor;

    @ManyToOne
    @JoinColumn(name="categoria_id")
    private CategoriaEntity categoria;


    private Double precio;




}
