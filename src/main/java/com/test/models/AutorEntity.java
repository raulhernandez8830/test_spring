package com.test.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Entity
@Table(name="autores")
@Setter @Getter @AllArgsConstructor @NoArgsConstructor
public class AutorEntity {

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     private Integer id;
     private String nombre;
     private String pais;
     private Date fechaNacimiento;

     @OneToMany(mappedBy = "autor", cascade= CascadeType.ALL, fetch = FetchType.LAZY)
     @JsonBackReference
     List<LibroEntity> libros;



}
