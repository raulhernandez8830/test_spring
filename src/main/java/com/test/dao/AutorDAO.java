package com.test.dao;

import com.test.models.AutorEntity;
import org.springframework.data.repository.CrudRepository;

public interface AutorDAO extends CrudRepository<AutorEntity, Integer> {
}
