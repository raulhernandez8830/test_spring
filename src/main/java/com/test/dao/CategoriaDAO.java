package com.test.dao;

import com.test.models.CategoriaEntity;
import org.springframework.data.repository.CrudRepository;

public interface CategoriaDAO extends CrudRepository<CategoriaEntity, Integer> {
}
